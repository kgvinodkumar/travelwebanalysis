package com.afkl.exercises.spring.webanalysis;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.concurrent.Callable;
import java.util.concurrent.ThreadLocalRandom;

import static java.math.RoundingMode.HALF_UP;
import static java.util.Locale.ENGLISH;
import static org.springframework.web.bind.annotation.RequestMethod.GET;




@RestController
@RequestMapping("/webreport")
public class WebanalysisController {

    private final WebreportRepository repository;

    @Autowired
    public WebanalysisController(WebreportRepository repository) {
        this.repository = repository;
    }

    @RequestMapping(method = GET)
    public Callable<PagedResources<Resource<Location>>> list(@RequestParam(value = "lang", defaultValue = "en") String lang, Pageable<Location> pageable) {
        return () -> pageable.partition(repository.list(Locale.forLanguageTag(lang)));
    }

    @RequestMapping(value = "/{key}", method = GET)
    public Callable<HttpEntity<Location>> show(@RequestParam(value = "lang", defaultValue = "en") String lang, @PathVariable("key") String key) {
        return () -> {
            Thread.sleep(ThreadLocalRandom.current().nextLong(200, 800));
            return repository.get(Locale.forLanguageTag(lang), key)
                    .map(l -> new ResponseEntity<>(l, OK))
                    .orElse(new ResponseEntity<>(NOT_FOUND));
        };
    }

    @RequestMapping(method = GET, params = "term")
    public Callable<HttpEntity<PagedResources<Resource<Location>>>> find(@RequestParam(value = "lang", defaultValue = "en") String lang,
                                                                         @RequestParam("term") String term,
                                                                         Pageable<Location> pageable){
        return () -> repository.find(Locale.forLanguageTag(lang), term)
                .map(l -> new ResponseEntity<>(pageable.partition(l), OK))
                .orElse(new ResponseEntity<>(NOT_FOUND));
    }

}

